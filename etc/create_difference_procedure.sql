delimiter //
USE hdbpp//
CREATE PROCEDURE difference_attr_day (IN att_id INT, IN aday DATE, OUT difference DOUBLE)
proc_label: BEGIN
DECLARE table_name VARCHAR(255);
DECLARE ts DATETIME;
DECLARE val DOUBLE;

SELECT CONCAT('att_',data_type) FROM att_conf c JOIN att_conf_data_type d ON c.att_conf_data_type_id = d.att_conf_data_type_id WHERE c.att_conf_id = att_id INTO table_name;

IF table_name = 'att_scalar_devdouble_ro' THEN

    SELECT val_since_last_date
    FROM (
    SELECT
            mintable.minvalue - @val AS val_since_last_date,
            @val := mintable.minvalue minvalue,
            mintable.mindate
        FROM (
            SELECT mindate,minvalue
            FROM ( -- get initial values of previous value
                SELECT @val:=MIN(value_r) FROM att_scalar_devdouble_ro WHERE att_conf_id=att_id AND DATE(data_time) < aday GROUP BY(DATE(data_time)) ORDER BY DATE(data_time) DESC LIMIT 1
            ) AS dummy
            CROSS JOIN
            ( -- get all data of att_id in aday
                SELECT att_conf_id, DATE(data_time) as mindate, MIN(value_r) as minvalue FROM att_scalar_devdouble_ro WHERE att_conf_id = att_id AND (DATE(data_time)=aday 
                OR DATE(data_time)=DATE(aday - interval 1 day)) GROUP BY(DATE(data_time))
            )
            AS a
            UNION -- add line with memorized aday and val in case of empty result in this day
            SELECT aday,@val
        ) AS mintable
    ) AS result WHERE mindate=aday LIMIT 1 INTO difference;

END IF;

-- SELECT CONCAT_WS(' ',table_name,'att_conf_id=',att_id,' Time-weighted difference=',difference);

END
//
delimiter ;


delimiter //
USE hdbpp//
CREATE PROCEDURE difference_day (IN aday DATE)
proc_label: BEGIN
DECLARE bDone INT;
DECLARE diff DOUBLE;
DECLARE att_id INT;
DECLARE diff_period INT;
DECLARE name VARCHAR(255);

DECLARE curs CURSOR FOR SELECT att_conf_id, period, alias FROM att_conf_aggregate WHERE TIMESTAMPDIFF(SECOND, last_time, NOW()) > 2*period-60 AND type='difference';
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = TRUE;

OPEN curs;
    SET bDone = FALSE;
    read_loop: LOOP
        FETCH curs INTO att_id,diff_period,name;
        IF bDone THEN
              LEAVE read_loop;
          END IF;
        -- SELECT CONCAT_WS(' ','FOUND',name,'with id',att_id,'and period',avg_period);
        CALL difference_attr_day(att_id,aday,@diff);
        INSERT INTO att_scalar_aggregate_ro VALUES (att_id,aday,@diff);
        UPDATE att_conf_aggregate SET last_time=aday WHERE att_conf_id = att_id;
        -- SELECT CONCAT_WS(' ',name,'att_conf_id=',att_id,' updated Time-weighted average=',@diff);
        -- DO SLEEP(1);
    END LOOP;
CLOSE curs;

END
//
delimiter ;



delimiter //
USE hdbpp//
CREATE PROCEDURE difference ()
proc_label_ttl: BEGIN
DECLARE a DATE;


SET a = '2023-01-01';

WHILE a < CURRENT_DATE() DO
    CALL difference_day(a);
    SET a = DATE_ADD(a, INTERVAL 1 DAY);
END WHILE;
END
//
delimiter ;

