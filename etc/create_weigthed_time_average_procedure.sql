delimiter //
USE hdbpp//
CREATE PROCEDURE time_weighted_average_attr_day (IN att_id INT, IN aday DATE, OUT average DOUBLE)
proc_label: BEGIN
DECLARE table_name VARCHAR(255);
DECLARE ts DATETIME;
DECLARE val DOUBLE;

SELECT CONCAT('att_',data_type) FROM att_conf c JOIN att_conf_data_type d ON c.att_conf_data_type_id = d.att_conf_data_type_id WHERE c.att_conf_id = att_id INTO table_name;

IF table_name = 'att_scalar_devdouble_ro' THEN

    SELECT SUM(weighted_sum) / SUM(ts_diff) as time_weighted_average -- our derived average
    FROM (
        SELECT CASE
                WHEN prev_val IS NULL THEN NULL -- 
                ELSE (prev_val)  * (ts_diff) END AS weighted_sum, ts_diff
            FROM ( -- get
            SELECT @ts prev_ts_e, TIMESTAMPDIFF(MICROSECOND, @ts, a.data_time) AS ts_diff,  @ts:=a.data_time as ts_e,a.att_conf_id,@val prev_val, @val:=a.value_r curr_val
        FROM ( -- get initial values of previous value and previous timestamp:
            SELECT @val:=value_r, @ts:=IF(data_time>aday,data_time,aday) FROM att_scalar_devdouble_ro WHERE att_conf_id=att_id AND DATE(data_time) < aday ORDER BY data_time DESC LIMIT 1
        ) AS dummy
        CROSS JOIN
        ( -- get all data of att_id in aday
            SELECT att_conf_id, data_time, value_r FROM att_scalar_devdouble_ro WHERE att_conf_id=att_id AND DATE(data_time) = aday
            UNION (SELECT att_id as att_conf_id, DATE_ADD(aday, INTERVAL 1 DAY) as data_time, 0 as value_r) ORDER BY data_time -- add final data_time of the day, not interested in next value
        ) AS a
        ORDER BY a.data_time
        ) AS ws
    ) AS wavg INTO average;

END IF;

-- SELECT CONCAT_WS(' ',table_name,'att_conf_id=',att_id,' Time-weighted average=',average);

END
//
delimiter ;


delimiter //
USE hdbpp//
CREATE PROCEDURE time_weighted_aggregate_day (IN aday DATE)
proc_label: BEGIN
DECLARE bDone INT;
DECLARE wavg DOUBLE;
DECLARE att_id INT;
DECLARE avg_period INT;
DECLARE name VARCHAR(255);

DECLARE curs CURSOR FOR SELECT att_conf_id, period, alias FROM att_conf_aggregate WHERE TIMESTAMPDIFF(SECOND, last_time, NOW()) > 2*period-60 AND type='average';
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = TRUE;

OPEN curs;
    SET bDone = FALSE;
    read_loop: LOOP
        FETCH curs INTO att_id,avg_period,name;
        IF bDone THEN
              LEAVE read_loop;
          END IF;
        -- SELECT CONCAT_WS(' ','FOUND',name,'with id',att_id,'and period',avg_period);
        CALL time_weighted_average_attr_day(att_id,aday,@wavg);
        INSERT INTO att_scalar_aggregate_ro VALUES (att_id,aday,@wavg);
        UPDATE att_conf_aggregate SET last_time=aday WHERE att_conf_id = att_id;
        -- SELECT CONCAT_WS(' ',name,'att_conf_id=',att_id,' updated Time-weighted average=',@wavg);
        -- DO SLEEP(1);
    END LOOP;
CLOSE curs;

END
//
delimiter ;



delimiter //
USE hdbpp//
CREATE PROCEDURE time_weighted_average ()
proc_label_ttl: BEGIN
DECLARE a DATE;


SET a = '2020-01-01';

WHILE a < CURRENT_DATE() DO
    CALL time_weighted_average_day(a);
    SET a = DATE_ADD(a, INTERVAL 1 DAY);
END WHILE;
END
//
delimiter ;

