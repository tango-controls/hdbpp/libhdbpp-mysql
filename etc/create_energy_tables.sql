USE hdbpp;
CREATE TABLE IF NOT EXISTS att_scalar_aggregate_ro
(
att_conf_id INT UNSIGNED NOT NULL,
data_time DATETIME(6) NOT NULL,
value_r DOUBLE DEFAULT NULL,
PRIMARY KEY(att_conf_id, data_time)
) ENGINE=InnoDB COMMENT='Scalar Data Aggregation Table';

USE hdbpp;
CREATE TABLE att_conf_aggregate (
  att_conf_id int(10) unsigned NOT NULL,
  period INT UNSIGNED NOT NULL DEFAULT 86400,
  type VARCHAR(64) NOT NULL DEFAULT 'average',
  last_time DATETIME(6) NOT NULL,
  alias VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (att_conf_id)
) ENGINE=InnoDB COMMENT='Attribute Data Aggregation Configuration Table';
